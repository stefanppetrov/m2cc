#ifndef SHARED_H
#define SHARED_H

#include <nodes.h>
#include <types.h>
#include <string>
#include <iterator>

typedef std::string IDENT;

IDENT get_ident(NODE* ast);
int get_int_literal(NODE* ast);
TYPE_SYMBOL get_type(NODE* ast);

// Convenience functions for working with the STL more painlessly

using std::rbegin;
using std::rend;
using std::begin;
using std::end;
using std::cbegin;
using std::cend;

template <typename T>
struct reverse_wrapper { T& iterable; };

template <typename T>
auto begin (reverse_wrapper<T> cont) { return rbegin(cont.iterable); }

template <typename T>
auto end (reverse_wrapper<T> cont) { return rend(cont.iterable); }

template <typename T>
reverse_wrapper<T> reverse (T&& iterable) { return { iterable }; }

template <typename T, template <typename, typename...> class Container, typename... Args> 
Container<T, Args...>& append(Container<T, Args...>& c, const Container<T, Args...>& a){
   c.insert(end(c), cbegin(a), cend(a)); 
   return c;
}


#endif
