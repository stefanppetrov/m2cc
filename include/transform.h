#ifndef TRANSFORM_H
#define TRANSFORM_H

template<typename Preimage, typename Image>
class Transform {
public:
  virtual Image transform(Preimage preimage) = 0; 
  virtual ~Transform() {}
};

template<typename In, typename Hidden, typename Out>
class Pipeline;

template<typename In, typename Hidden, typename Out>
Pipeline<In, Hidden, Out> operator>>(Transform<In, Hidden>& prev,
                                     Transform<Hidden, Out>& next) {
  return Pipeline<In,Hidden,Out>(&prev, &next);
}

template<typename In, typename Hidden, typename Out>
class Pipeline: public Transform<In, Out> {
  using FIRST = Transform<In, Hidden>;
  using SECOND = Transform<Hidden, Out>;

  private:
    
    FIRST  *first;
    SECOND *second;

  public:
    Pipeline( FIRST *f,  SECOND *s): first(f), second(s) {}

    virtual Out transform(In in) {
      auto middle = first->transform(in);
      return second->transform(middle);
    }

};

#endif
