#ifndef TAC_H
#define TAC_H

#include <string>
#include <vector>
#include <iostream>

#include <C.tab.h>
#include <token.h>
#include <shared.h>

enum class OP {
  ADD = 0,
  SUB,
  MULT,
  DIV,
  MOD,
  EQ,
  NE,
  LT,
  GT,
  GE,
  LE,
  NOT,
  PUT,
  GLOBAL,
  FUN,
  END,
  VAR,
  RETURN,
  IF,
  GOTO,
  PARAM,
  NOOP,
  SLINK,
  CALL,
  ARG,
};

struct TacOperand {
  bool isConstant;
  IDENT ident;
  int value;

  TacOperand(IDENT ident): ident(ident), isConstant(false){}
  TacOperand(int val): value(val), isConstant(true){}
  TacOperand(TOKEN* tok): isConstant(tok->type == CONSTANT) {
    if (isConstant)
      value = tok->value;
    else
      ident = tok->lexeme;
  } 

  std::string toString(void) const;

  friend std::ostream& operator<<(std::ostream&, const TacOperand&);
};

typedef std::vector<TacOperand> TACO_LIST;

struct TacInstr {
  OP op;
  IDENT label;
  TACO_LIST operands;

  TacInstr(OP op = OP::NOOP);
  TacInstr(OP op, TacOperand op1); 
  TacInstr(OP op, TacOperand op1, TacOperand op2);
  TacInstr(OP op, TacOperand op1, TacOperand op2, TacOperand op3);
  TacInstr(OP op, std::initializer_list<TacOperand> operands);
 
  std::string toString(void) const;
  
  friend std::ostream& operator<<(std::ostream&, const TacInstr&);
};

typedef std::vector<TacInstr> TAC_LIST;

#endif
