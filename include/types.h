#ifndef MYTYPES_H
#define MYTYPES_H

#include <string>
#include <memory>
#include <vector>

enum TYPE_SYMBOL{
  INT_T = 0,
  CLOSURE_T,
  WILDCARD_T
};

bool matches(TYPE_SYMBOL a, TYPE_SYMBOL b);

typedef std::vector<TYPE_SYMBOL> TYPELIST;

struct Type {
  virtual TYPE_SYMBOL getType() = 0;
  virtual ~Type(){}
};

struct IntType : public Type {
  virtual TYPE_SYMBOL getType();
};

struct ClosureType : public Type {
  std::vector<TYPE_SYMBOL> argTypes;
  TYPE_SYMBOL rvType;
  
  virtual TYPE_SYMBOL getType() ;

  ClosureType();

  ClosureType(TYPE_SYMBOL rvType, const TYPELIST& argtypes);
};

struct WildcardType : public Type {
  virtual TYPE_SYMBOL getType();
};

typedef Type* TYPE_PTR;
typedef IntType* INT_T_PTR;
typedef ClosureType* CLOSURE_T_PTR;
typedef WildcardType* WILDCARD_T_PTR;


#endif
