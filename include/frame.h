#ifndef FRAME_H
#define FRAME_H

#include <values.h>
#include <shared.h>

#include <string>
#include <vector>
#include <memory>
#include <unordered_map>
#include <stdexcept>
#include <iostream>

class Frame;

typedef std::shared_ptr<Frame> FRAME_PTR;

class Frame{

  FRAME_PTR parent;
  std::unordered_map<IDENT, VALUE_PTR> bindings;

  Frame* findEnvFor(IDENT ident);

public:
  
  Frame(FRAME_PTR parent): parent(parent) {
    if(parent.get() == this)
      throw std::invalid_argument("An environment can't be its own parent");
  }
  
  Frame(): parent(nullptr) {}
  
  ~Frame() {}
  
  bool isBound(IDENT ident);
  
  VALUE_PTR& get(IDENT ident);

  void bind(IDENT ident, VALUE_PTR value);
  void rebind(IDENT ident, VALUE_PTR value);

  const FRAME_PTR& getParent() const;
  void setParent(FRAME_PTR);

  friend std::ostream& operator<<(std::ostream&, const Frame&);
};
 
#endif
