#ifndef TAC_OPTIMIZER_H 
#define TAC_OPTIMIZER_H 

#include <tac.h>
#include <transform.h>

#include <unordered_map>
#include <vector>
#include <string>

struct TacBlock;
struct TacFunction;

typedef TAC_LIST::iterator TacPosition;
typedef std::vector<TacBlock> BlockList;
typedef std::vector<int> BlockIndxList;
typedef std::unordered_map<int, BlockIndxList> CtrlFlowGraph;
typedef std::unordered_map<IDENT, int> LabelIndex; 
typedef std::vector<TacFunction> FuncList;

struct TacBlock{
  TAC_LIST tacs;

  TacBlock(TacPosition& start);
  TacBlock(const TacBlock&) = default;
  TacBlock& operator=(const TacBlock&) = default;
  TacBlock(TacBlock&&) = default;
  TacBlock& operator=(TacBlock&&) = default;
};
std::ostream& operator<<(std::ostream&, const TacBlock&);

struct TacFunction{
  TacInstr start, end;
  BlockList blocks;

  CtrlFlowGraph contr;
  LabelIndex labelIndx;

  TacFunction(TacPosition& start);
  TacFunction(const TacFunction&) = default;
  TacFunction& operator=(const TacFunction&) = default;
  TacFunction(TacFunction&&) = default;
  TacFunction& operator=(TacFunction&&) = default;
};
std::ostream& operator<<(std::ostream&, const TacFunction&);

struct TacProgram{
  TAC_LIST globals;
  FuncList funcs;

  TacProgram(TAC_LIST&);
  TacProgram(const TacProgram&) = default;
  TacProgram& operator=(const TacProgram&) = default;
  TacProgram(TacProgram&&) = default;
  TacProgram& operator=(TacProgram&&) = default;
};
std::ostream& operator<<(std::ostream&, const TacProgram&);

class TacOptimizer: public Transform<TAC_LIST, TAC_LIST> {
  TAC_LIST tacs;

public:
  
  TAC_LIST transform(TAC_LIST input);
};

#endif
