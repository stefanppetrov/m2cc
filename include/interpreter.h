#ifndef INTERPETER_H
#define INTERPETER_H

#include <frame.h>
#include <values.h>
#include <types.h>
#include <nodes.h>
#include <values_impl.h>
#include <transform.h> 

#include <iostream>
#include <stdexcept>

class ControlFlow: public std::exception {};

class Break: public ControlFlow {};

class Continue: public ControlFlow {};

class Return: public ControlFlow{
private:
  VALUE_PTR result;
public:
  Return(const VALUE_PTR&);
  VALUE_PTR getResult() const;
};

class Interpreter : public Transform<NODE*, int> {
  FRAME_PTR global;
    
  public:
    Interpreter();    
    virtual int transform(NODE*);
    
  protected:
    FRAME_PTR& eval(NODE*, FRAME_PTR&);
    FRAME_PTR& eval_declaration(NODE* ast, FRAME_PTR&);
    FRAME_PTR& eval_declarators(NODE* , FRAME_PTR&);
    VALUE_PTR eval_expression(NODE*, FRAME_PTR&);
    VALUE_PTR eval_rvalue(NODE*, FRAME_PTR&);
    VALUE_PTR eval_binary_op(NODE*, FRAME_PTR&);
    VALUE_PTR eval_unary_op(NODE*, FRAME_PTR&);
    FRAME_PTR& eval_if_stmt(NODE*, FRAME_PTR&);
    FRAME_PTR& eval_while_loop(NODE*, FRAME_PTR&);
    FRAME_PTR& eval_func_def(NODE*, FRAME_PTR&);
    void eval_func_def_args(NODE*, ARGDEF_T&);
    VALUE_PTR eval_func_call(NODE*, FRAME_PTR&);
    void eval_func_args(NODE*, ARGDEF_T&, FRAME_PTR&, FRAME_PTR&);
    VALUE_PTR call_func(CLOSURE_PTR, FRAME_PTR&); 
};

#endif
