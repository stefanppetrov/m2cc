#ifndef COMPILER_H
#define COMPILER_H

#include <tac.h>
#include <transform.h>

#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <memory>

typedef TAC_LIST::const_iterator TAC_STREAM;
typedef std::unordered_map<IDENT, int> MEM_MAP;
typedef std::vector<std::string> ASM_LIST;
typedef std::unordered_map<IDENT, IDENT> REG_MAP;
typedef std::unordered_set<IDENT> GLOBALS_SET;

constexpr size_t WORD_SIZE = 4;
constexpr size_t ARG_REGS = 4;
constexpr size_t CONTROL_REGS = 4;
constexpr size_t CALLER_AR_RSRV = CONTROL_REGS;
constexpr size_t CALLEE_AR_RSRV = 2;

class GlobalContext{
  GLOBALS_SET globals;
  GLOBALS_SET functions;
public:

  bool put(IDENT, bool);
  bool isGlobal(TacOperand) const;
  bool isFunc(TacOperand) const;
  std::string get(TacOperand) const;

  static std::string decorated(IDENT);
};

class Context{
  int argoffset;
  int offset;
  MEM_MAP mmap;
  MEM_MAP argmap;
  REG_MAP regmap;
  const GlobalContext* globalCtxt;
  const Context* parentCtxt;

public:
  Context(const Context&) = delete;

  Context(const Context*, const GlobalContext*);
  Context(Context&&);

  int getOffset() const; 
  int getArgOffset() const;
  std::string getOffsetStr() const; 
  std::string getArgOffsetStr() const;
 
  int put(IDENT ident);
  int putArg(IDENT ident);

  bool isReg(const TacOperand&) const;
  bool isArg(TacOperand) const;
  bool isLocal(TacOperand) const;
  const Context* getParentFor(TacOperand) const;
  bool isCapture(TacOperand) const;
  bool isGlobal(TacOperand) const;
  bool isFunc(TacOperand) const;
  IDENT getLoc(TacOperand op, bool noreg=false) const;

  void setGlobalCtxt(const GlobalContext*);
  void setParentCtxt(const Context*);
  const Context* getParent() const;
  
  bool saveOnReturn;
};

typedef std::unique_ptr<Context> OWNED_CTXT;
typedef std::unordered_map<IDENT, OWNED_CTXT> OWNED_CTXT_MAP;

class Compiler: public Transform<TAC_LIST, ASM_LIST> {

  OWNED_CTXT_MAP ctxtMap; 
  Context* ctxt;

  GlobalContext globalCtxt;
  ASM_LIST output;

public:
  
  Compiler();

  virtual ASM_LIST transform(TAC_LIST);

private:

  ASM_LIST compile(const TAC_LIST&);
  ASM_LIST compile_fun(TAC_STREAM&);
  ASM_LIST compile_bin_op(TacInstr);
  ASM_LIST compile_put(TacInstr);
  ASM_LIST compile_if(TacInstr);
  ASM_LIST compile_return(TacInstr);
  ASM_LIST compile_fun_call(TAC_STREAM&);
  ASM_LIST compile_param(TacInstr, int);
  ASM_LIST compile_static_link(TacInstr);

  ASM_LIST emit_allocation(size_t);
  // Evict argument registers and saved registers to memory
  ASM_LIST emit_save_state();
  // Restore arguments and saved registers from memory
  ASM_LIST emit_load_state();

  // Emits code for loading an operand into the
  // specified register if needed.
  ASM_LIST emit_load_into(TacOperand, IDENT&);

  // Transfers a value from a register to the current
  // storage for the specified operand.
  ASM_LIST emit_save_from(TacOperand, const IDENT&);

  const Context* getParentCtxt(IDENT);
};

#endif
