#ifndef SYMTABLE_H
#define SYMTABLE_H

#include <types.h>
#include <shared.h>

#include <string>
#include <vector>
#include <memory>
#include <unordered_map>
#include <iostream>

class SymTable;

typedef SymTable* SYMTABLE_PTR;

class SymTable{

  SymTable* parent;
  std::unordered_map<IDENT, TYPE_PTR> bindings;

  SymTable* findEnvFor(IDENT ident);

public:
  
  SymTable(SYMTABLE_PTR parent=nullptr): parent(parent) {}
  SymTable(const SymTable&) = delete;

  bool isBound(IDENT ident);
  
  TYPE_PTR get(IDENT ident);
  
  void declare(TYPE_PTR type, IDENT ident);
  
  SYMTABLE_PTR getParent() const;
  void setParent(SYMTABLE_PTR);

  friend std::ostream& operator<<(std::ostream&, const SymTable&);
};
 
#endif
