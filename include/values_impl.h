#ifndef VALUES_IMPL
#define VALUES_IMPL

#include <values.h>
#include <frame.h>
#include <nodes.h>
#include <shared.h>

typedef std::unordered_map<int, IDENT> ARGDEF_T;

struct Int: public Value{
  int value;
  
  Int(): value(0){}
  
  Int(int value): value(value){}
  
  virtual TYPE_SYMBOL getType() {return INT_T;}
  virtual Int* clone(){return new Int(value);}
};

struct Closure: public Value{
  FRAME_PTR env;
  ARGDEF_T argDefs;
  NODE* ast;
 
  Closure(const ARGDEF_T& argDefs, NODE* ast, FRAME_PTR env): ast(ast), argDefs(argDefs), env(env) {}
  Closure(): ast(nullptr), env(new Frame()) {} 

  virtual TYPE_SYMBOL getType() {return CLOSURE_T;}
  virtual Closure* clone() {return new Closure(argDefs, ast, env);}
};

typedef std::shared_ptr<Int> INT_PTR;
typedef std::shared_ptr<Closure> CLOSURE_PTR;

#endif
