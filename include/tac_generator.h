#ifndef TAC_COMPILER_H
#define TAC_COMPILER_H

#include <tac.h>
#include <transform.h>
#include <vector>

class TacGenerator : public Transform<NODE*, TAC_LIST>{

public:
  
  TacGenerator(IDENT);
  TacGenerator();

  virtual TAC_LIST transform(NODE*);

  TacGenerator(TacGenerator&&) = default;
  TacGenerator(const TacGenerator&) = delete;
  TacGenerator& operator=(const TacGenerator&) = delete;

private:
  TAC_LIST& gen_global(NODE*);
  TAC_LIST& gen_global_declaration(NODE*);
  TAC_LIST& gen_global_declarators(NODE*);
  TAC_LIST& gen_local(NODE*, IDENT);
  TAC_LIST& gen_declaration(NODE*);
  TAC_LIST& gen_declarators(NODE*);
  TAC_LIST& gen_func_def(NODE*);
  TacOperand gen_expression(NODE*);
  TacOperand gen_rvalue(NODE*);
  TacOperand gen_binary_op(NODE*);
  TacOperand gen_unary_op(NODE*);
  TAC_LIST& gen_if_stmt(NODE*, IDENT);
  TAC_LIST& gen_while_loop(NODE*, IDENT);
  TAC_LIST& gen_return(NODE*);
  TacOperand gen_fun_call(NODE*); 
  TAC_LIST& gen_func_def_nested(NODE*, IDENT);

  IDENT nextLabel();
  TacOperand nextTemp();
  IDENT addNs(IDENT, IDENT);
  
  TAC_LIST output;
  std::vector<NODE*> pendingInner;

  int tmpCount;
  int labelCount;
  IDENT ns;
};

#endif
