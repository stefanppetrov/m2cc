#ifndef TYPE_TABLE_H
#define TYPE_TABLE_H

#include <types.h>
#include <vector>

class TypeTable {

  std::vector<TYPE_PTR> types;

public:

  TypeTable();
  ~TypeTable();

  TypeTable(TypeTable&&) = default;
  TypeTable(const TypeTable&) = delete;

  void addType(TYPE_PTR);
  TYPE_PTR translate(TYPE_SYMBOL);
};

#endif
