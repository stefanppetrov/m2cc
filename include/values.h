#ifndef VALUES_H
#define VALUES_H

#include <types.h>
#include <shared.h>

#include <memory>
#include <unordered_map>

struct Value{
  virtual ~Value(){}
  virtual Value* clone() = 0;
  virtual TYPE_SYMBOL getType() = 0;
};

struct Int;
struct Closure;

typedef std::shared_ptr<Value> VALUE_PTR;
typedef std::shared_ptr<Int> INT_PTR;
typedef std::shared_ptr<Closure> CLOSURE_PTR;

#endif
