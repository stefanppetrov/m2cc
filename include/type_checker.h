#ifndef TYPE_PTR_CHEKER_H
#define TYPE_PTR_CHEKER_H

#include <nodes.h>
#include <symtable.h>
#include <types.h>
#include <type_table.h>
#include <transform.h>

#include <vector>

class TypeChecker: public Transform<NODE*, NODE*>{
  SymTable symtable;
  TYPE_PTR ctxtType;
  
  public:
    virtual NODE* transform(NODE*);    
    
    TypeChecker(): symtable(nullptr), ctxtType(nullptr) {}
  private:

    TypeChecker(SymTable* parentTable, TYPE_PTR ctxtType=nullptr):
      symtable(parentTable), ctxtType(ctxtType) {}

    TypeChecker(TypeChecker& parent, TYPE_PTR ctxtType=nullptr):
      symtable(&parent.symtable), ctxtType(ctxtType) {}

    void check(NODE*);
    void check_declaration(NODE*);
    void check_declarators(TYPE_PTR, NODE*);
    TYPE_PTR check_expression(NODE*);
    TYPE_PTR check_rvalue(NODE*);
    TYPE_PTR check_binary_op(NODE*);
    TYPE_PTR check_unary_op(NODE*);
    void check_func_def(NODE*);
    TYPELIST check_func_def_args(NODE*, SymTable&);
    TYPE_PTR check_func_call(NODE*);
    TYPELIST check_func_call_args(NODE*);
    void check_return_stmt(NODE*);
    void check_if_stmt(NODE*);
    void check_while_loop(NODE*);

    static TypeTable& getTypeTable();    
};

#endif
