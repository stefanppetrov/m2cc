CXX := clang++

SRC_DIR = src
BUILD_DIR = build
LIB_DIR = cparser
BIN_DIR = bin

OUTPUT := $(BIN_DIR)/mycc

SRC := $(wildcard $(SRC_DIR)/*.cpp)
OBJ := $(SRC:$(SRC_DIR)/%.cpp=$(BUILD_DIR)/%.o)
INC := $(addprefix -I, $(wildcard */include))
LIBS_LOCAL := $(LIB_DIR)/libcparser.a
STD := c++14

CPPFLAGS += --std=$(STD) -Iinclude $(INC)
LIBS += $(LIBS_LOCAL)

.PHONY: all
all:	$(OUTPUT)

clean:
	rm $(OBJ) $(OUTPUT); \
	$(MAKE) -C $(LIB_DIR) clean

$(OUTPUT): $(OBJ) $(LIBS_LOCAL) 
	$(CXX) -o $@ $^ $(CPPFLAGS) $(LIBS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp 
	$(CXX) $(CPPFLAGS) -c $< -o $@
	
$(LIBS_LOCAL):
	$(MAKE) -C $(LIB_DIR)
	
depend:	
	${CC} -M $(SRC) > .deps
	cat Makefile .deps > makefile
	
$(shell mkdir -p $(BUILD_DIR))
$(shell mkdir -p $(BIN_DIR))
