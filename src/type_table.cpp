#include <type_table.h>

TypeTable::TypeTable():
  types({new IntType, new ClosureType, new WildcardType}) {}

TypeTable::~TypeTable() {
  for(auto typep : types)
    delete typep;
}

void TypeTable::addType(TYPE_PTR ptr) {
  types.push_back(ptr);
}

TYPE_PTR TypeTable::translate(TYPE_SYMBOL typesym) {
  switch(typesym) {
    case INT_T:
      return types[0];
    case CLOSURE_T:
      return types[1];
    default:
      return types[2];
  }
}
