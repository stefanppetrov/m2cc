#include <cparser.h>
#include <interpreter.h>
#include <compiler.h>
#include <type_checker.h>
#include <tac_generator.h>
#include <tac_optimizer.h>

#include <cstdio>
#include <fstream>
#include <unistd.h>

bool compile = false;
std::string outputFile = "output";

class TacPrinter: public Transform<TAC_LIST, TAC_LIST>
{
  public:
  
  virtual TAC_LIST transform(TAC_LIST tacs)
  {
    auto tacFile = std::ofstream(outputFile+".tac");
    for(auto& tac : tacs) {
      tacFile << tac << std::endl;
    }
    return tacs;
  }
};

int main(int argc, char** argv){
  
  int c;
  while((c = getopt(argc, argv, "cf:")) != -1){
    switch(c) {
      case 'c':
        compile = true; break;
      case 'f': {
        std::string inputFile = optarg;
        size_t lastDot = inputFile.find_last_of(".");
        outputFile = inputFile.substr(0, lastDot);
        freopen(inputFile.c_str(), "r", stdin);
        break;
      }
      default: break;
    }
  }

  try{

    NODE* node = parse(0);

    TypeChecker checker;
    
    if(!compile){
      Interpreter interpreter;
      int result = (checker >> interpreter).transform(node);
      std::cout << "Returned with result: " << result << "\n";
    }
    else {
      TacGenerator gen;
      TacPrinter p;
      Compiler comp;
      TacOptimizer opt;
      auto stage1 = checker >> gen;
      auto stage2 = stage1 >> p;
      auto stage3 = stage2 >> opt;
      auto all = stage3 >> comp;
      auto result = all.transform(node);

      auto assemblyFile = std::ofstream(outputFile+".s");

      for(auto& instr: result)
        assemblyFile << instr;
    }
    return 0;

  } catch(const std::exception& e) {
    std::cout << e.what() << std::endl;
    return 1;
  }
}
