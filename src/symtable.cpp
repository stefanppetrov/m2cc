#include <symtable.h>
#include <values.h>

#include <stdexcept>

SymTable* SymTable::findEnvFor(IDENT ident){
  if(isBound(ident))
    return this;

  if(parent)
    return parent->findEnvFor(ident);

  return nullptr;
}

bool SymTable::isBound(IDENT ident) {
  return bindings.find(ident) != bindings.end();
}

TYPE_PTR SymTable::get(IDENT ident) {
  SymTable* locum = findEnvFor(ident);
  if(!locum){
    throw std::invalid_argument("Unbound identifier: " + ident);      
  }
  return locum->bindings.at(ident);
}

void SymTable::declare(TYPE_PTR type, IDENT ident) {
  
  if(isBound(ident)) {
    throw std::invalid_argument("Symbol already bound: " + ident);
  } 
  bindings.emplace(ident, type);
}

SYMTABLE_PTR SymTable::getParent() const {
  return parent;
}

void SymTable::setParent(SYMTABLE_PTR parent) {
  this->parent = parent;
}

std::ostream& operator<<(std::ostream& o, const SymTable& e) {
  if(e.getParent())
    o << e.getParent() << std::endl;

  for(const auto& bind: e.bindings) {
    TYPE_SYMBOL type = bind.second->getType();
    o << bind.first << ": (type: " << type << ")\n";
  }
  return o;
}
