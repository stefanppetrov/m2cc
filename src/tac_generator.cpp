#include <shared.h>
#include <tac_generator.h>
#include <cparser.h>
#include <iostream>

TacGenerator::TacGenerator(IDENT ns)
  : tmpCount(0), labelCount(0), ns(ns) {}

TacGenerator::TacGenerator(): TacGenerator("") {}

TAC_LIST TacGenerator::transform(NODE* ast) {
  gen_global(ast);
  return output;
}

TAC_LIST& TacGenerator::gen_global(NODE* ast) {
  if (!ast) return output;
  switch(ast->type) {
    case '~':
      return gen_global_declaration(ast);
    case 'D':
      // Except for functions;
      return gen_func_def(ast);
    default:
      gen_expression(ast);
      return output;
  }
}

TAC_LIST& TacGenerator::gen_global_declaration(NODE* ast) {
  NODE* leftAst = ast->left;                        

  switch(leftAst->type) {                           

    case '~': { 
      gen_global(leftAst);               
      return gen_global(ast->right);                 
    }
    case LEAF: {                                    
      return gen_global_declarators(ast->right);     
    } 
    case 'D': {
      gen_global(leftAst);
      return gen_global(ast->right);
    }

    default:
      throw std::invalid_argument("Bad global symbol");
  }  
}

TAC_LIST&  TacGenerator::gen_global_declarators(NODE* ast) {
  switch(ast->type){
    
    case LEAF: { // Found an identifier  
      IDENT ident = get_ident(ast); 
      output.push_back(TacInstr{OP::GLOBAL, {ident, 0}});
      return output;
    }
    
    case '=' : {
      auto ident = get_ident(ast->left); 
      auto value = TacOperand((TOKEN*)(ast->right->left));
      output.push_back(TacInstr{OP::GLOBAL, {ident, value}});
      return output;
    }
    
    case ',' : {
      gen_global_declarators(ast->left);
      return gen_global_declarators(ast->right);
    }
    
    default: 
      return output;
  }
}

TAC_LIST& TacGenerator::gen_local(NODE* ast, IDENT localNs) {
  if (!ast) return output;
  switch(ast->type) {
    case ';':
      gen_local(ast->left, localNs);
      return gen_local(ast->right, localNs);
    case '~':
      return gen_declaration(ast);
    case IF:
      return gen_if_stmt(ast, localNs);
    case WHILE:
      return gen_while_loop(ast, localNs);
    case RETURN:
      return gen_return(ast);
    case 'D':
      std::cout << localNs << "\n";
      return gen_func_def_nested(ast, localNs);
    default:
      gen_expression(ast);
      return output;
  }
}

TAC_LIST& TacGenerator::gen_func_def(NODE* ast) {
  IDENT funName = get_ident(ast->left->right->left);
  NODE* paramAst = ast->left->right->right;
  auto funType = get_type(ast->left->left);

  std::vector<NODE*> paramstk;

  if (paramAst && paramAst->type!=LEAF){
    while(paramAst->type == ',') {
      print_tree(paramAst);
      paramstk.push_back(paramAst->right);
      paramAst = paramAst->left; 
    }
    paramstk.push_back(paramAst);
  }

  const auto& nsName = addNs(funName, ns);
  TacInstr tacInstr{OP::FUN, {nsName, paramstk.size(), funType}};
  output.push_back(tacInstr);

  for(auto& paramNode : reverse(paramstk)) {
    IDENT name = get_ident(paramNode->right);
    output.push_back(TacInstr{OP::ARG, name});
  }
  // std::cout << funName << "\n";
  gen_local(ast->right, funName);

  output.push_back(TacInstr{OP::END,{}});
  
  // Generate nested functions;
  while(!pendingInner.empty()){
    auto& last = pendingInner.back();
    append(output, TacGenerator(nsName).transform(last));
    pendingInner.pop_back();
  }

  return output;
}

TAC_LIST&  TacGenerator::gen_declaration(NODE* ast) {
  return gen_declarators(ast->right);
}

TAC_LIST&  TacGenerator::gen_declarators(NODE* ast) {
  switch(ast->type){
    
    case LEAF: { // Found an identifier  
      IDENT ident = get_ident(ast); 
      output.push_back(TacInstr{OP::VAR, {ident}});
      output.push_back(TacInstr{OP::PUT, {0, ident}});
      return output;
    }
    
    case '=' : {
      IDENT ident = get_ident(ast->left); 
      output.push_back(TacInstr{OP::VAR, {ident}});
      gen_expression(ast);
      return output;
    }
    
    case ',' : {
      gen_declarators(ast->left);
      return gen_declarators(ast->right);
    }
    
    default: 
      return output;
  }
}

TacOperand TacGenerator::gen_expression(NODE* ast) {

  switch(ast->type) {
    case LEAF:
      return gen_rvalue(ast);
    case APPLY:
      return gen_fun_call(ast);
    default: 
      if(ast->right)
        return gen_binary_op(ast);
      return gen_unary_op(ast);
  }
}

TacOperand TacGenerator::gen_rvalue(NODE* ast) {
  
  return TacOperand{(TOKEN*)(ast->left)};
}

TacOperand TacGenerator::gen_binary_op(NODE* ast) {

  if (ast->type == '=') {
    IDENT ident = get_ident(ast->left);
    auto rhs = gen_expression(ast->right);
    auto nextTac = TacInstr{OP::PUT, {rhs, ident}};
    auto& lastTac = output.back();
    if (lastTac.operands.back().ident == rhs.ident && rhs.ident[0] == '$') // Remove redundant copies
      lastTac.operands.back().ident = ident;
    else
      output.push_back(nextTac);
    
    return ident;
  }  

  auto lhs = gen_expression(ast->left);
  auto rhs = gen_expression(ast->right);
  auto result = nextTemp();
  output.push_back(TacInstr{OP::VAR, {result}});
  
  switch(ast->type) {
    case ',':
      return rhs;

    case '+':
      output.push_back(TacInstr{OP::ADD, {lhs, rhs, result}});break;

    case '-':
      output.push_back(TacInstr{OP::SUB, {lhs, rhs, result}});break;

    case '*':
      output.push_back(TacInstr{OP::MULT, {lhs, rhs, result}});break;

    case '/':
      output.push_back(TacInstr{OP::DIV, {lhs, rhs, result}});break;

    case '%':
      output.push_back(TacInstr{OP::MOD, {lhs, rhs, result}});break;
     
    case '>':
      output.push_back(TacInstr{OP::GT, {lhs, rhs, result}});break;
     
    case '<':
      output.push_back(TacInstr{OP::LT, {lhs, rhs, result}});break;
     
    case EQ_OP:
      output.push_back(TacInstr{OP::EQ, {lhs, rhs, result}});break;
     
    case NE_OP:
      output.push_back(TacInstr{OP::NE, {lhs, rhs, result}});break;
     
    case GE_OP:
      output.push_back(TacInstr{OP::GE, {lhs, rhs, result}});break;
     
    case LE_OP:
      output.push_back(TacInstr{OP::LE, {lhs, rhs, result}});break;
      
    default:
      throw std::invalid_argument("Unknown binary operator");
  }
  return result;
}

TacOperand TacGenerator::gen_unary_op(NODE* ast) {

  auto lhs = gen_expression(ast->left);
  auto result = nextTemp();
  
  switch(ast->type) {
    case '+':
      return lhs;
    case '-':
      output.push_back(TacInstr{OP::LE, {0, lhs, result}});break;
    case '!':
      output.push_back(TacInstr{OP::NOT, {lhs, result}});break;
    default:
      throw std::invalid_argument("Unknown unary operator");
  }
  return result;
}

TAC_LIST& TacGenerator::gen_if_stmt(NODE* ast, IDENT localNs) {
  auto cond = gen_expression(ast->left);
  NODE *body = ast->right, *elseBody = nullptr;
  IDENT ifEnd = nextLabel(), elseStart, jumpDest = ifEnd;

  if(body->type == ELSE){
    elseBody = body->right;
    body = body->left;
      
    if(elseBody){
      elseStart = nextLabel(); 
      jumpDest = elseStart;
    }
  }
  output.push_back(TacInstr{OP::IF, cond, jumpDest});
  
  gen_local(body, localNs);

  if(elseBody ) {
    output.push_back(TacInstr{OP::GOTO, ifEnd});
    int nextPos = output.size();
    gen_local(elseBody, localNs);
    output[nextPos].label = elseStart;
  }

  TacInstr noop(OP::NOOP);
  noop.label = ifEnd;
  output.push_back(noop);

  return output;
}

TAC_LIST& TacGenerator::gen_while_loop(NODE* ast, IDENT localNs) {
  IDENT loopStart=nextLabel(), loopEnd=nextLabel();
  int nextPos = output.size();
  auto cond = gen_expression(ast->left);

  output.push_back(TacInstr{OP::IF, cond, loopEnd});
  // Do this now to ensure we're actually adding the label to something
  output[nextPos].label = loopStart;

  gen_local(ast->right, localNs);
  
  output.push_back(TacInstr{OP::GOTO, loopStart});

  TacInstr noop(OP::NOOP);
  noop.label = loopEnd;
  output.push_back(noop);

  return output;  
}

TacOperand TacGenerator::gen_fun_call(NODE* ast) {
  const auto& result = nextTemp(); 
  auto funExpr = gen_expression(ast->left);
  auto paramAst = ast->right;
  TACO_LIST paramResults;  

  output.push_back(TacInstr{OP::VAR, result});

  if(paramAst){
    while(paramAst->type == ','){ 
      paramResults.push_back(gen_expression(paramAst->right));
      paramAst = paramAst->left;
    }
    paramResults.push_back(gen_expression(paramAst));

    for(auto& subres : reverse(paramResults))
      output.push_back(TacInstr{OP::PARAM, subres});
  }
  output.push_back(TacInstr{OP::CALL, funExpr, result});

  return result;
}

TAC_LIST& TacGenerator::gen_func_def_nested(NODE* ast, IDENT localNs) {
  IDENT funName = get_ident(ast->left->right->left);
  pendingInner.push_back(ast);

  std::string fullName = addNs(addNs(funName, localNs), ns);
  output.push_back(TacInstr{OP::SLINK, fullName, funName});
  return output;
}

IDENT TacGenerator::addNs(IDENT name, IDENT localNs) {

  if(localNs.empty())
    return name;
  return name + "." + localNs;
}

TAC_LIST& TacGenerator::gen_return(NODE* ast){
  auto rval = gen_expression(ast->left);
  output.push_back(TacInstr{OP::RETURN, rval});
  return output;
}

TacOperand TacGenerator::nextTemp() {
  IDENT ident = "$" + std::to_string(tmpCount++);
  return TacOperand{ident};
}

IDENT TacGenerator::nextLabel() {
  return "L" + std::to_string(labelCount++);
}
