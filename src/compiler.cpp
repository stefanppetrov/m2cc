#include <compiler.h>
#include <iostream>
#include <iterator>

std::string getMipsOp(OP op){
  switch(op){
    case OP::MOD:  return "rem";
    case OP::DIV:  return "div";
    case OP::ADD:  return "add";
    case OP::SUB:  return "sub";
    case OP::MULT: return "mul";
    case OP::EQ:   return "seq";
    case OP::NE:   return "sne";
    case OP::GT:   return "sgt";
    case OP::LT:   return "slt";
    case OP::GE:   return "sge";
    case OP::LE:   return "sle";
    default: throw std::invalid_argument("Not a binary operation");
  }
}

std::string GlobalContext::decorated(IDENT a) {
  return "__glob__." + a;
}

bool GlobalContext::put(IDENT ident, bool isFunc) {
  bool success = globals.emplace(ident).second;
  if(!success)
    return false;
  if(isFunc){
    success = functions.emplace(ident).second;
    if(!success) {
      globals.erase(ident);
      return false;
    }
  }
  return true;
}

bool GlobalContext::isGlobal(TacOperand op) const {
  return globals.find(op.ident) != globals.end();
}

bool GlobalContext::isFunc(TacOperand op) const {
  return functions.find(op.ident) != functions.end();
}

std::string GlobalContext::get(TacOperand op) const {
  if(isGlobal(op))
    return decorated(op.ident);
  throw std::invalid_argument("Unrecognized global variable or function");
}

Context::Context(const Context* parentCtxt, const GlobalContext* globalCtxt): 
  parentCtxt(parentCtxt),
  offset(CALLEE_AR_RSRV),
  argoffset(CALLER_AR_RSRV),
  globalCtxt(globalCtxt) {}

Context::Context(Context&& moved):
  parentCtxt(moved.parentCtxt),
  offset(CALLEE_AR_RSRV),
  argoffset(CALLER_AR_RSRV),
  globalCtxt(moved.globalCtxt),
  mmap(moved.mmap),
  argmap(moved.argmap),
  regmap(moved.regmap) {}

int Context::getOffset() const {return offset;}
std::string Context::getOffsetStr() const {return std::to_string(offset);}

int Context::getArgOffset() const {return argoffset;}
std::string Context::getArgOffsetStr() const {return std::to_string(argoffset);}

int Context::put(IDENT ident) {
  if(mmap.emplace(ident, offset).second)
    offset++;
  return offset;
}

int Context::putArg(IDENT ident){
  if(argoffset < CALLER_AR_RSRV + ARG_REGS) 
    regmap.emplace(ident, "$a" + std::to_string(argoffset-CALLER_AR_RSRV));  
  
  bool rval = argmap.emplace(ident, argoffset).second;
  
  if(rval)argoffset++;
  return argoffset;
}

bool Context::isFunc(TacOperand op) const {
  return globalCtxt->isFunc(op);
}

bool Context::isReg(const TacOperand& op) const {
  return regmap.find(op.ident) != regmap.end();
}

bool Context::isArg(TacOperand op) const {
  return argmap.find(op.ident) != argmap.end();
}

bool Context::isLocal(TacOperand op) const {
  return mmap.find(op.ident) != mmap.end();
}

const Context* Context::getParentFor(TacOperand op) const {
  const Context* ancestor = parentCtxt;
  while(ancestor)
    if(ancestor->isLocal(op) || ancestor->isArg(op))
      return ancestor;
    else
      ancestor = ancestor->parentCtxt;
  return nullptr;
}

const Context* Context::getParent() const {
  return parentCtxt;
}

bool Context::isCapture(TacOperand op) const {
  return getParentFor(op) != nullptr;
}

bool Context::isGlobal(TacOperand op) const {
  return globalCtxt->isGlobal(op); 
}

IDENT Context::getLoc(TacOperand op, bool noreg) const {
  if(!noreg && isReg(op))
    return regmap.at(op.ident);

  if(isLocal(op))
    return std::to_string(mmap.at(op.ident)*WORD_SIZE)+"($fp)";

  if(isArg(op))
    return std::to_string(argmap.at(op.ident)*WORD_SIZE)+"($s0)";

  const Context* ancestor = getParentFor(op);
  if(ancestor){
    size_t offset = 0; 

    if(ancestor->isLocal(op))
      offset = ancestor->mmap.at(op.ident);    
    else if(ancestor->isArg(op))
      offset = ancestor->argmap.at(op.ident);

    return std::to_string(offset*WORD_SIZE)+"($v1)";
  }

  if(isGlobal(op))
    return globalCtxt->get(op); 

  if(op.isConstant)
    return op.toString();

  throw std::invalid_argument("Invalid identifier: " + op.toString());
}

void Context::setGlobalCtxt(const GlobalContext* globalCtxt) {
  this->globalCtxt = globalCtxt;
}

void Context::setParentCtxt(const Context* ctxt) {
  this->parentCtxt = ctxt;
}


Compiler::Compiler(): ctxt(nullptr) { }

ASM_LIST Compiler::transform(TAC_LIST input) {
  return compile(input);
}

ASM_LIST Compiler::compile(const TAC_LIST& tacs) {
  ASM_LIST globals;
  ASM_LIST functions;
  std::vector<TAC_STREAM> functionTACs;
  auto tacIter = tacs.cbegin();

  // Set up data segment and global context 
  globals.push_back("  .data\n"); 
  while(tacIter != tacs.end()){
    switch(tacIter->op){
      case OP::FUN: {
        const auto& fname = (tacIter)->operands[0].toString();
        globalCtxt.put(fname, true);
        globals.push_back(globalCtxt.get(fname) + ": .word " + fname + ", 0\n");
        functionTACs.push_back(tacIter++);
        break;
      }
      case OP::GLOBAL: {
        auto& def= *(tacIter++);
        auto op1 = def.operands[0].toString(), op2 = def.operands[1].toString();
        globalCtxt.put(op1, false);
        globals.push_back(globalCtxt.get(op1) + ": .word " + op2 + "\n");
        break;
      }
      default:
        tacIter++;
        break;
    }
  }

  // Set up text segment and local contexts
  functions.push_back("  .text\n");
  for(auto& tacIter : functionTACs) {
    append(functions, compile_fun(tacIter));
  }

  append(output, globals);
  return append(output, functions);
}

ASM_LIST Compiler::compile_bin_op(TacInstr instr) {
  ASM_LIST o;
  auto &op1=instr.operands[0], &op2=instr.operands[1], &op3=instr.operands[2]; 
  IDENT sreg1="$t0", sreg2="$t1", dreg="$t2";

  // Load the first operand into a register if needed
  append(o, emit_load_into(op1, sreg1));

  // Load the second operand into a register if needed
  append(o, emit_load_into(op2, sreg2));

  if(ctxt->isReg(op3))
      dreg = ctxt->getLoc(op3);

  o.push_back(getMipsOp(instr.op) + " " + 
              dreg + ", " + 
              sreg1 + ", " + 
              sreg2 + "\n");

  if(!ctxt->isReg(op3))
    o.push_back("sw " + dreg + ", " + ctxt->getLoc(op3) + "\n");
  return o;
}

ASM_LIST Compiler::compile_put(TacInstr instr) {
  ASM_LIST o;
  auto &op1=instr.operands[0], &op2=instr.operands[1];

  IDENT sreg="$t0";
  append(o, emit_load_into(op1, sreg));
  append(o, emit_save_from(op2, sreg));
  return o;
}

ASM_LIST Compiler::compile_if(TacInstr instr){
  ASM_LIST o;
  auto &cond = instr.operands[0], &dest = instr.operands[1];

  IDENT condReg = "$t0";
  append(o, emit_load_into(cond, condReg));

  o.push_back("beq " + condReg + ", $0, " + dest.toString() + "\n");
  o.push_back("nop\n");
  return o;
}

ASM_LIST Compiler::compile_return(TacInstr instr) {
  ASM_LIST o;
  auto& rval = instr.operands[0];

  IDENT rvalReg = "$v0";
  append(o, emit_load_into(rval, rvalReg));

  if(rvalReg != "$v0") {
    o.push_back("move $v0, "+rvalReg+"\n");
  }

  if(ctxt->saveOnReturn) // We are returning a closure, save state just in case
    append(o, emit_save_state());

  o.push_back( "jr $ra\n");
  return o;
}

ASM_LIST Compiler::compile_fun(TAC_STREAM& tacIter) {
  ASM_LIST local, result;
  const auto& funName = tacIter->operands[0].toString(); 
  const auto& saveOnReturn = tacIter->operands[0].value; // For functional results

  const Context* parentCtxt = getParentCtxt(funName);
  ctxtMap.emplace(funName, std::make_unique<Context>(parentCtxt, &globalCtxt));
  ctxt = ctxtMap[funName].get();
  ctxt->saveOnReturn = saveOnReturn > 0;

  result.push_back(funName + ": # " + funName + "\n");
 
  while((++tacIter)->op != OP::END){
    if(tacIter->label.size())
      local.push_back(tacIter->label + ":\n");

    ASM_LIST res;
    switch(tacIter->op){
      case OP::ARG:
        ctxt->putArg(tacIter->operands[0].ident);
        break;
      case OP::VAR:
        ctxt->put(tacIter->operands[0].ident);
        break;
      case OP::MOD: 
      case OP::DIV: 
      case OP::ADD: 
      case OP::SUB: 
      case OP::MULT: 
      case OP::EQ:   
      case OP::NE:   
      case OP::GT:   
      case OP::LT:    
      case OP::GE:     
      case OP::LE:
        res = compile_bin_op(*tacIter); break;
      case OP::PUT:
        res = compile_put(*tacIter); break;
      case OP::IF:
        res = compile_if(*tacIter); break;
      case OP::RETURN:
        res = compile_return(*tacIter); break;
      case OP::PARAM:
      case OP::CALL:
        res = compile_fun_call(tacIter); break;
      case OP::SLINK:
        res = compile_static_link(*tacIter); break;
      default: break;
    }
    append(local, res);
  }

  append(result, emit_allocation(ctxt->getOffset()));
  result.push_back(
      "sw $s0, 0($v0)\n" // Needed for closures 
      "sw $v1, 4($v0)\n"// Needed for closures
      "move $fp, $v0\n");
  append(result, local);
  result.push_back( "jr $ra\n");

  tacIter++;
  return result;
}

ASM_LIST Compiler::compile_param(TacInstr instr, int paramCnt){
  ASM_LIST param;
  auto& op = instr.operands[0];
  
  auto paramOffset = std::to_string(WORD_SIZE * (paramCnt + CALLER_AR_RSRV)),
       cntstr = std::to_string(paramCnt),
       opStr = ctxt->getLoc(op, true), // For parameters assume things have been evicted
       opcode = std::string("lw ");
  
  if(op.isConstant)
    opcode = "li ";
  else if(ctxt->isGlobal(op) && ctxt->isFunc(op))
    opcode = "la ";

  if(paramCnt < 4) {
      param.push_back(opcode + "$a" + cntstr + ", " + opStr + "\n");
  }
  else {
      param.push_back(opcode + "$t0, " + opStr + "\n");
      param.push_back("sw $t0, " + paramOffset+ "($v0)\n");
  }
  return param;
}

ASM_LIST Compiler::compile_static_link(TacInstr instr) {
  ASM_LIST results;
  auto &closure = instr.operands[0], &localClosure = instr.operands[1];
  ctxt->put(localClosure.ident);
  append(results, emit_allocation(2));
  results.push_back(
      "la $t0, " + closure.ident + "\n" // We can use $s0, since there is a restore for it
      "sw $t0, 0($v0) # Save closure label\n" 
      "sw $fp, 4($v0) # Static link with current AR\n"
      "sw $v0, " + ctxt->getLoc(localClosure.ident) + " # Persist closure pointer to memory\n" 
  );
  return results;
}

ASM_LIST Compiler::compile_fun_call(TAC_STREAM& tacIter) {
  TAC_STREAM start = tacIter;
  ASM_LIST params, result;

  // Generate code for loading parameters but don't load it in the result just yet. 
  int paramPos=0, paramCnt=0;
  while(tacIter->op == OP::PARAM){
    append(params, compile_param(*tacIter, paramCnt));
    tacIter++, paramCnt++;
  }
  int ctxtSize = paramCnt + CALLER_AR_RSRV; // Size of frame to allocate ( 

  auto &callee = tacIter->operands[0], 
       &resultLoc = tacIter->operands[1];
  const auto& calleeName = ctxt->getLoc(callee);

  result = emit_allocation(ctxtSize);

  // Save own parameters and locals, but not temporaries
  append(result, emit_save_state());
 
  // Find out where we need to load the environment from
  IDENT calleeReg = "$t1";  
  append(result, emit_load_into(callee, calleeReg));

  // Save control
  result.push_back(
    "sw $ra, 0($v0)\n"
    "sw $fp, 4($v0)\n" 
    "sw $v1, 8($v0)\n"
    "sw $s0, 12($v0)\n"
    "lw $t0, 0(" + calleeReg + ")\n" 
    "lw $v1, 4(" + calleeReg + ")\n" 
  );
  // Load parameters into callee AR frame
  append(result, params);

  // Pass control onto callee, and restore control after return
  result.push_back(
    "move $s0, $v0\n"
    "jalr $ra, $t0 # Call " + callee.toString() + "\n"
    "lw $ra, 0($s0)\n"
    "lw $fp, 4($s0)\n" 
    "lw $v1, 8($s0)\n"
    "lw $s0, 12($s0) # Complete caller state restore\n"
  );

  // Restore own parameters and locals, but not temporaries
  append(result, emit_load_state());

  bool isReg = ctxt->isReg(resultLoc);
  
  if(isReg){
    IDENT dreg = ctxt->getLoc(resultLoc);
    result.push_back("move v0, " + dreg + "\n");  
  } 
  else {
    result.push_back("sw $v0, " + ctxt->getLoc(resultLoc) + "\n");
  }
  return result;
}

ASM_LIST Compiler::emit_save_state() {
  ASM_LIST result;
 
  // Caller arguments are saved in caller's caller AR;
  // Only save the first 4 for now, assume the others will be
  // already saved (before implementing optimizations).
  int argoffset = ctxt->getArgOffset();
  int maxargreg = CALLER_AR_RSRV + ARG_REGS;
  for(int i=CALLER_AR_RSRV; i<argoffset && i<maxargreg ; i++)
  {
    auto offset = std::to_string(i * WORD_SIZE),
         istr = std::to_string(i-CALLER_AR_RSRV);
    result.push_back("sw $a"+ istr +", "+ offset + "($s0)\n");
  }

  /*
  for(int i=0; i<8; i++)
  {
    std::string offset = std::to_string((i+3) * WORD_SIZE),
                istr = std::to_string(i);
    result.push_back("sw $s"+istr+", "+ offset + "($fp)\n");
  }*/

  return result;
}

ASM_LIST Compiler::emit_load_state() {
  ASM_LIST result;
 
  // Caller arguments are saved in caller's caller AR;
  // Only save the first 4 for now, assume the others will be
  // already saved (before implementing optimizations).
  int argoffset = ctxt->getArgOffset();
  int maxargreg = CALLER_AR_RSRV + 4;
  for(int i=CALLER_AR_RSRV; i<argoffset && i<maxargreg ; i++)
  {
    auto offset = std::to_string(i * WORD_SIZE),
         istr = std::to_string(i-CALLER_AR_RSRV);
    result.push_back("lw $a"+ istr +", "+ offset + "($s0)\n");
  }

  /*
  // Restore saved non-control registers, except for $s0 for now
  for(int i=1; i<8; i++)
  {
    std::string offset = std::to_string((i+3) * WORD_SIZE),
                istr = std::to_string(i);
    result.push_back("lw $s"+istr+", "+ offset + "($fp)\n");
  }
  */

  return result;
}

ASM_LIST Compiler::emit_allocation(size_t words){
  ASM_LIST allocInstr;
  allocInstr.push_back(
      "addi $sp, $sp, -4 # Memory allocation block\n"
      "sw $a0, 0($sp) # Save arg 0\n" // Store arg 0 in s0 
      "li $v0, 9 # sbrk\n"
      "li $a0, " + std::to_string(words * WORD_SIZE) + "\n"
      "syscall\n"  
      "lw $a0, 0($sp) # Restore arg 0\n" // Restore arg 0 from s0
      "addi $sp, $sp, 4 # End memory allocation block\n"
  );

  return allocInstr;
}

ASM_LIST Compiler::emit_load_into(TacOperand op, IDENT& regHint) {
  ASM_LIST o;

  IDENT loadCode = "lw ", 
        opLoc = ctxt->getLoc(op);

  if(ctxt->isReg(op)){
    regHint = opLoc; 
  }
  else {
    auto captureCtxt = ctxt->getParentFor(op);

    bool local = ctxt->isLocal(op) || ctxt->isArg(op);

    if(op.isConstant) {
      loadCode = "li ";
      o.push_back(loadCode + regHint + ", " + opLoc + "\n");
    }
    else if (local) {
      o.push_back(loadCode + regHint + ", " + opLoc + "\n");
    }
    else if (captureCtxt) {
      // Find how many captured AR we have to go through
      auto currentCtxt = ctxt->getParent();
      bool arglookup = !captureCtxt->isLocal(op);
      bool slowLookup = currentCtxt != captureCtxt || arglookup;
      
      while(currentCtxt != captureCtxt) {
        currentCtxt = currentCtxt->getParent();
        o.push_back("lw $v1, 4($v1)\n");
      }
      // If it's in the arguments 1 more load is needed
      if(arglookup)
        o.push_back("lw $v1, 0($v1)\n");

      o.push_back(loadCode + regHint + ", " + opLoc + "\n");

      if(slowLookup)
        o.push_back("lw $v1, 4($fp) # Restore static link after slow lookup\n");
    } 
    else if (ctxt->isGlobal(op) && ctxt->isFunc(op)) {
      loadCode = "la ";
      o.push_back(loadCode + regHint + ", " + opLoc + "\n");
    }
  }
  return o;
}

ASM_LIST Compiler::emit_save_from(TacOperand op, const IDENT& src){
  ASM_LIST o;

  if(op.isConstant){
    throw std::invalid_argument("Integer constant does represent storage");
  }
  else if(ctxt->isReg(op)){
    IDENT dest = ctxt->getLoc(op);
    o.push_back("move " + dest + ", " + src + "\n");  
  } 
  else { 
    auto captureCtxt = ctxt->getParentFor(op);
    bool local = ctxt->isLocal(op) || ctxt->isArg(op);
    bool slowLookup = false;

    if(!local && captureCtxt){
      auto currentCtxt = ctxt->getParent();
      bool arglookup = !captureCtxt->isLocal(op);
      slowLookup = currentCtxt != captureCtxt || arglookup;

      while(currentCtxt != captureCtxt) {
        currentCtxt = currentCtxt->getParent();
        o.push_back("lw $v1, 4($v1)\n");
      }
      // If it's in the arguments 1 more load is needed
      if(arglookup)
        o.push_back("lw $v1, 0($v1)\n");
    }

    o.push_back("sw " + src + ", " + ctxt->getLoc(op) + "\n");

    if(slowLookup)
      o.push_back("lw $v1, 4($fp) # Restore static link after slow lookup\n");
  }
  return o;
}

const Context* Compiler::getParentCtxt(IDENT funName) {
  size_t dotPos = funName.find(".");
  if(dotPos == IDENT::npos)
    return nullptr;

  auto ctxtName = funName.substr(dotPos+1, IDENT::npos);
  return ctxtMap.at(ctxtName).get();
}
