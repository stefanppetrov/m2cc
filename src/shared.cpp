#include <shared.h>
#include <C.tab.h>

#include <stdexcept>

IDENT get_ident(NODE* ast) {
  TOKEN* token = (TOKEN*)ast->left;
  std::string errMsg = "Invalid identifier token: ";
  
  if(IDENTIFIER != token->type) {
      throw std::invalid_argument(errMsg + token->lexeme );
  }
  
  return token->lexeme;
}

int get_int_literal(NODE* ast){
  TOKEN* token = (TOKEN*)ast->left;
  std::string errMsg = "Invalid int literal token: ";
  
  if(CONSTANT != token->type) {
      throw std::invalid_argument(errMsg + token->lexeme );
  }
  
  return token->value;
}

TYPE_SYMBOL get_type(NODE* ast) {
    TOKEN* token = (TOKEN*)ast->left;
    std::string errMsg = "Unknown type: ";
    
    switch(token->type){
      case INT: 
        return INT_T;
      case FUNCTION: 
        return CLOSURE_T;
      default: 
        throw std::invalid_argument(errMsg + token->lexeme );
    }
}
