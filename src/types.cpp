#include <types.h>

bool matches(TYPE_SYMBOL a, TYPE_SYMBOL b) {
  return a == b || a == WILDCARD_T || b == WILDCARD_T;
}


TYPE_SYMBOL IntType::getType(){
  return INT_T;
}

ClosureType::ClosureType(): rvType(WILDCARD_T) {}
TYPE_SYMBOL ClosureType::getType() {
  return CLOSURE_T;
}

ClosureType::ClosureType(TYPE_SYMBOL rvType, const TYPELIST& argtypes)
  : rvType(rvType), argTypes(argtypes) {}

TYPE_SYMBOL WildcardType::getType() { return WILDCARD_T;}
