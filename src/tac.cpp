#include <sstream>
#include <tac.h>

std::string opstr(OP op) {
  switch(op){
    case OP::ADD:    return "ADD";
    case OP::SUB:    return "SUB";
    case OP::MULT:   return "MULT";
    case OP::DIV:    return "DIV";
    case OP::MOD:    return "MOD";
    case OP::EQ:     return "EQ";
    case OP::NE:     return "NE";
    case OP::GT:     return "GT";
    case OP::LT:     return "LT";
    case OP::GE:     return "GE";
    case OP::LE:     return "LE";
    case OP::PUT:    return "PUT";
    case OP::NOT:    return "NOT";
    case OP::GLOBAL: return "GLOBAL";
    case OP::FUN:    return "FUN";
    case OP::END:    return "END";
    case OP::VAR:    return "VAR";
    case OP::RETURN: return "RETURN";
    case OP::IF:     return "IF";
    case OP::GOTO:   return "GOTO";
    case OP::PARAM:  return "PARAM";
    case OP::NOOP:   return "NOOP";
    case OP::SLINK:  return "SLINK";
    case OP::CALL:   return "CALL";
    case OP::ARG:    return "ARG";
    default:  return "???";
  };
}

std::string TacOperand::toString(void) const {
  if(isConstant)
    return std::to_string(value);
  return ident;
}

std::ostream& operator<<(std::ostream& o, const TacOperand& op){
  if(op.isConstant)
    return o<<op.value;
  return o<<op.ident;
}

TacInstr::TacInstr(OP op): op(op) {}

TacInstr::TacInstr(OP op, std::initializer_list<TacOperand> operands)
  : op(op), operands(operands) {}

TacInstr::TacInstr(OP op, TacOperand op1) 
  : op(op) {
    operands.push_back(op1);
  }

TacInstr::TacInstr(OP op, TacOperand op1, TacOperand op2)
  : op(op) {
    operands.push_back(op1);
    operands.push_back(op2);
  }

TacInstr::TacInstr(OP op, TacOperand op1, TacOperand op2, TacOperand op3)
  : op(op) {
    operands.push_back(op1);
    operands.push_back(op2);
    operands.push_back(op3);
  }

std::string TacInstr::toString() const {
  std::ostringstream os;
  if(label.size())
    os << label << ": ";
  os << opstr(op);
  for (const auto& operand: operands)
    os << " " << operand;
  return os.str();
}

std::ostream& operator<<(std::ostream& o, const TacInstr& inst){
  return o<<inst.toString();
}
