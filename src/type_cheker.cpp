#include <type_checker.h>
#include <C.tab.h>
#include <cparser.h>
#include <token.h>

#include <algorithm>

NODE* TypeChecker::transform(NODE* ast) {
  check(ast);
  return ast;
}

void TypeChecker::check(NODE* ast){
  if (!ast) return;
  
  switch(ast->type) {
    case BREAK:
    case CONTINUE:
      return;
    case RETURN:
      check_return_stmt(ast); return;
    case '~':    
      check_declaration(ast); return;
    case 'D':
      check_func_def(ast); return;
    case IF:
      check_if_stmt(ast); return;
    case WHILE:
      check_while_loop(ast); return;
    case ';':
      check(ast->left);
      check(ast->right); return;
    default: 
      check_expression(ast); break;
  }
}

void TypeChecker::check_declaration(NODE* ast){
  NODE* leftAst = ast->left;
  
  switch(leftAst->type) {
    
    case '~': // Declarations followed by something else...
      check_declaration(leftAst);
      check(ast->right);
      return;
      
    case LEAF: {
      auto type = getTypeTable().translate(get_type(leftAst));
      check_declarators(type, ast->right);
      return;
    }
    default:
      check(ast->left);
      check(ast->right);
  }
}

void TypeChecker::check_declarators(TYPE_PTR type, NODE* ast){
  switch(ast->type){
    
    case ',' : {
      check_declarators(type, ast->left);
      check_declarators(type, ast->right);
      return;
    }
    
    case '=' : { 
  
      IDENT ident = get_ident(ast->left);
      TYPE_PTR rtype = check_expression(ast->right);
      
      if(!matches(type->getType(), rtype->getType())){
        throw std::logic_error("Mismatched types in declaration assignment of " + ident);
      }

      symtable.declare(type, ident);
      return;
    }
    
    case LEAF: { // Found an identifier  
      symtable.declare(type, get_ident(ast));
      return;
    }
    
    default: 
      return;
  }
}

TYPE_PTR TypeChecker::check_expression(NODE* ast){
  
  if(!symtable.getParent()){
    if (ast->type != LEAF && (((TOKEN*)(ast->left)) -> type != CONSTANT ) ) {
        throw std::invalid_argument("Non-constant expression in global assignment");
    }
  }

  switch(ast->type) {
    case LEAF:
      return check_rvalue(ast);
    case APPLY:
      return check_func_call(ast);
    default:
      if(ast->right){
        return check_binary_op(ast);
      }
      throw std::invalid_argument("Unary operator corrupted by parser");
      // return TypeChecker::check_unary_op(ast, symtable);
  }
}

TYPE_PTR TypeChecker::check_rvalue(NODE* ast){
  TOKEN* token = (TOKEN*)ast->left;
  switch(token->type){
    case CONSTANT:
      return getTypeTable().translate(INT_T); 
    case IDENTIFIER:
      return symtable.get(token->lexeme);
    default:
      throw std::invalid_argument("Invalid rvalue token specified");
  }
}

TYPE_PTR TypeChecker::check_unary_op(NODE* ast){
  TYPE_PTR type = check_expression(ast->right);
  
  if (CLOSURE_T == type->getType()) {
    throw std::logic_error("Function type object in an arithmetic expression");
  }
  
  return type;
}

TYPE_PTR TypeChecker::check_binary_op(NODE* ast){
  auto rhs = check_expression(ast->right);
  auto lhs = check_expression(ast->left);

  auto lhsSym = lhs->getType();
  auto rhsSym = rhs->getType();
  if (!matches(lhsSym,rhsSym)) {
    throw std::logic_error("Type mismatch in binary expression");
  }
  
  if (ast->type == '=') {
    return lhs;
  }
  
  if (lhsSym == CLOSURE_T || rhsSym == CLOSURE_T) {
    throw std::invalid_argument("Function type object in an arithmetic expression");
  }
  
  return rhs;
}

void TypeChecker::check_func_def(NODE* ast) {
  auto signature = ast->left,
       body = ast->right,
       typeAst = signature->left,
       argdefAst = signature->right->right,
       nameAst = signature->right->left;

  SymTable argdefTable(&symtable);
  auto type = get_type(typeAst);
  auto funcName = get_ident(nameAst);
  const auto& argdefs = check_func_def_args(argdefAst, argdefTable);
  
  auto ptr = new ClosureType(type, argdefs);
  getTypeTable().addType(ptr);

  symtable.declare(ptr, funcName);
  TypeChecker(&argdefTable, getTypeTable().translate(type)).check(body);
}

TYPELIST TypeChecker::check_func_def_args(NODE* ast, SymTable& argdefTable){
  std::vector<NODE*> argstk;
  TYPELIST results;
  auto& typeTable = getTypeTable(); 

  if(!ast || ast->type == LEAF)
    return results;
    
  while(ast->type != '~') {
    auto left=ast->left, right=ast->right;
    argstk.push_back(right);
    ast = left;
  }
  argstk.push_back(ast);

  for(auto argdef: reverse(argstk)){
    auto typeSym = get_type(argdef->left);
    auto argIdent = get_ident(argdef->right);
    results.push_back(typeSym);
    
    TYPE_PTR type = typeTable.translate(typeSym);
    argdefTable.declare(type, argIdent);
  }
  return results;
}

TYPE_PTR TypeChecker::check_func_call(NODE* ast){ 
  
  IDENT funcName = "@anon";
  if(ast->left->type != APPLY)
    funcName = get_ident(ast->left);

  auto funcType = check_expression(ast->left);
  auto typeSym = funcType->getType();

  if(!matches(typeSym,CLOSURE_T)) {
    throw std::invalid_argument("Symbol " + funcName + "is not a function or a closure type");
  }

  auto realFuncType = dynamic_cast<CLOSURE_T_PTR>(funcType); 
  auto& paramTypes = realFuncType->argTypes; 

  const auto& argTypes = check_func_call_args(ast->right);

  auto rvType = getTypeTable().translate(realFuncType->rvType);
  if(realFuncType->rvType == WILDCARD_T)
    return rvType;

  bool argTypesMatch = 
    argTypes.size() == paramTypes.size() &&
    std::equal(argTypes.begin(), argTypes.end(), paramTypes.begin(), matches);

  if(!argTypesMatch){
    for(auto& a : argTypes){
      std::cout << a << " ";
    }
    std::cout << "\n";
    for(auto& a : paramTypes){
      std::cout << a << " ";
    }
    throw std::invalid_argument("Arguments mismatched for function call of " + funcName);
  }

  return rvType; 
}

TYPELIST TypeChecker::check_func_call_args(NODE* ast){
  TYPELIST argTypes; 
  if(ast){
    while(ast->type == ','){
      auto left = ast->left, right = ast->right;
      argTypes.push_back(check_expression(right)->getType());
      ast = left;
    }
    argTypes.push_back(check_expression(ast)->getType());
    std::reverse(argTypes.begin(), argTypes.end());
  }
  return argTypes;
}

void TypeChecker::check_return_stmt(NODE* ast){
  auto actualType = check_expression(ast->left);
  
  if(!ctxtType)
    return;

  auto ctxtTypeSym = ctxtType->getType(), actualTypeSym = actualType->getType();
  if(!matches(ctxtTypeSym, actualTypeSym)) {
    throw std::invalid_argument("Can't return " + 
                                std::to_string(actualTypeSym) + 
                                " in a function with return type " +
                                std::to_string(ctxtTypeSym));
  }
}

void TypeChecker::check_if_stmt(NODE* ast){
  NODE *cond = ast->left, 
       *body = ast->right, 
       *elseBody = nullptr;
  auto condType = check_expression(cond);
  
  if(!matches(condType->getType(), INT_T))
    throw std::invalid_argument("Functional expressions can't be converted to logical");

  if(body->type == ELSE)
    elseBody = body->right, body=body->left;

  check(body);
  if(elseBody)
    check(elseBody);
}

void TypeChecker::check_while_loop(NODE* ast){
  auto cond = ast->left, body = ast->right;
  auto condType = check_expression(cond);
  
  if(!matches(condType->getType(), INT_T))
    throw std::invalid_argument("Functional expressions can't be converted to logical");
  
  check(body);
}

TypeTable& TypeChecker::getTypeTable() {
  static TypeTable typeTable;
  return typeTable;
}

