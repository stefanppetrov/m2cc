#include <tac_optimizer.h>

template<template<typename, typename...> class T, typename... Args>  
std::ostream& operator<<(std::ostream& os, const T<Args...>& container)  
{    
    std::copy(std::begin(container),   
              std::end(container),   
              std::ostream_iterator<typename T<Args...>::value_type>(os, "\n")); 
    return os;
}   

TacBlock::TacBlock(TacPosition& start)
{

  do{
    tacs.push_back(*start);
    start++;
  } while(start->op != OP::IF &&
          start->op != OP::RETURN &&
          start->op != OP::GOTO &&
          start->op != OP::END &&
          !start->label.size());

  // Jumps (IF, RETURN, GOTO) should be part of the block
  // Jump targets or the end of the function are not however
  if(!start->label.size() && start->op != OP::END) {
    tacs.push_back(*start);
    start++;
  }
}

std::ostream& operator<<(std::ostream& os, const TacBlock& block) {
  return os << block.tacs;
}

TacFunction::TacFunction(TacPosition& start):
    start(*start) {

  while(start->op != OP::END) {
    blocks.push_back(TacBlock(start));
  }

  // END pseudo-instruction is part of the function
  this->end = *start++;
}

std::ostream& operator<<(std::ostream& os, const TacFunction& func) {

  return os << func.start << "\n" << func.blocks << func.end << "\n";
}

TacProgram::TacProgram(TAC_LIST& tacs) {
  auto start = tacs.begin(), end = tacs.end(); 
  while(start != end) {
    switch(start->op){
      case OP::GLOBAL:
        this->globals.push_back(*start++); break;
      case OP::FUN:
        this->funcs.push_back(TacFunction(start)); break;
      default:
        throw std::logic_error("Local TAC in global scope");
    }
  }
}


std::ostream& operator<<(std::ostream& os, const TacProgram& prog) {
  return os << prog.globals << "\n" << prog.funcs << "\n";
}

TAC_LIST TacOptimizer::transform(TAC_LIST input) {
  std::cout << TacProgram(input) << "\n"; 
  return input;
}

