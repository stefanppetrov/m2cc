#include <cparser.h>
#include <C.tab.h>

#include <shared.h>
#include <interpreter.h>

#include <memory>
#include <iostream>

Interpreter::Interpreter(): global(new Frame(nullptr)) { }

Return::Return(const VALUE_PTR& val){
  this->result = val;
}

VALUE_PTR Return::getResult() const {
  return this->result;
}

int Interpreter::transform(NODE* ast) {
  eval(ast, global);
  FRAME_PTR mainFrame(new Frame(global));
  auto main = std::dynamic_pointer_cast<Closure>(global->get("main"));
  auto result = std::dynamic_pointer_cast<Int>(call_func(main, mainFrame));
  if(result)
    return result->value;

  return 0;
}

FRAME_PTR& Interpreter::eval(NODE* ast, FRAME_PTR& env) {
  if (ast) switch(ast->type) {
    case '~':    
      eval_declaration(ast, env); break;
    case 'D':
      eval_func_def(ast, env); break;
    case IF:
      eval_if_stmt(ast, env); break;
    case WHILE:
      eval_while_loop(ast, env); break;
    case ';':
      eval(ast->left, env);
      eval(ast->right, env); break;
    // Control flow 
    case BREAK:
      throw Break();
    case CONTINUE:
      throw Continue();
    case RETURN:
      throw Return(eval_expression(ast->left, env));
    default:
      eval_expression(ast, env); break;
  }
  return env;
}

FRAME_PTR& Interpreter::eval_declaration(NODE* ast, FRAME_PTR& env){
  NODE* leftAst = ast->left;
  
  switch(leftAst->type) {
    
    case '~': // Declarations followed by something else...
      eval_declaration(leftAst, env);
      return eval(ast->right, env);
      
    case LEAF: {
      return eval_declarators(ast->right, env);
    }
    default:
      eval(leftAst, env);
      return eval(ast->right, env);
  }
}

FRAME_PTR& Interpreter::eval_declarators(NODE* ast, FRAME_PTR& env){

  switch(ast->type){
    
    case LEAF: { // Found an identifier  
      env->bind(get_ident(ast), VALUE_PTR());
      return env;
    }
    
    case '=' : {
      
      IDENT ident = get_ident(ast->left);
      
      VALUE_PTR rhs = eval_expression(ast->right, env);
      env->bind(ident, rhs);
      return env;
    }
    
    case ',' : {
      eval_declarators(ast->left, env);
      return eval_declarators(ast->right, env);
    }
    
    default: 
      return env;

  }
}

VALUE_PTR Interpreter::eval_rvalue(NODE* ast, FRAME_PTR& env){
  TOKEN* token = (TOKEN*)ast->left;
  switch(token->type){
    case CONSTANT:
      return VALUE_PTR(new Int(token->value));
    case IDENTIFIER:
      return VALUE_PTR(env->get(token->lexeme)->clone());
    default:
      throw std::invalid_argument("Invalid rvalue token specified");
  }
}

VALUE_PTR Interpreter::eval_expression(NODE* ast, FRAME_PTR& env) {
  switch(ast->type) {
    case LEAF:
      return eval_rvalue(ast, env);
    case APPLY:
      return eval_func_call(ast, env);
    default: 
      if(ast->right)
        return eval_binary_op(ast, env);
      return eval_unary_op(ast, env);
  }
 
}

VALUE_PTR Interpreter::eval_unary_op(NODE* ast, FRAME_PTR& env){
  auto lhs = std::dynamic_pointer_cast<Int>(eval_expression(ast->left, env));
  int val = lhs->value;
  switch(ast->type) {
    case '+':
      return lhs;
    case '-':
      (lhs)->value = -val;
      return lhs;
    case '!':
      (lhs)->value = !val;
      return lhs;
    default:
      throw std::invalid_argument("Unknown unary operator");
  }
}

VALUE_PTR Interpreter::eval_binary_op(NODE* ast, FRAME_PTR& env){
  if (ast->type == '=') {
    IDENT lvalue = get_ident(ast->left);
    VALUE_PTR rhs = eval_expression(ast->right, env);

    env->rebind(lvalue, rhs);
    return VALUE_PTR(rhs->clone());
  }

  VALUE_PTR lhs = eval_expression(ast->left, env);
  VALUE_PTR rhs = eval_expression(ast->right, env);
  
  auto ilhs = std::dynamic_pointer_cast<Int>(lhs);
  auto irhs = std::dynamic_pointer_cast<Int>(rhs);
  
  switch(ast->type) {
    case ',':
      ilhs->value = irhs->value; break;
    case '+':
      ilhs->value = ilhs->value + irhs->value;break;
    case '-':
      ilhs->value = ilhs->value - irhs->value; break;
    case '*':
      ilhs->value = (ilhs->value * irhs->value);break;
    case '/':
      ilhs->value = (ilhs->value / irhs->value);break;
    case '%':
      ilhs->value = (ilhs->value % irhs->value);break;
    case '>':
      ilhs->value = (ilhs->value > irhs->value);break;
    case '<':
      ilhs->value = (ilhs->value < irhs->value);break;
    case EQ_OP:
      ilhs->value = (ilhs->value == irhs->value);break;
    case NE_OP:
      ilhs->value = (ilhs->value != irhs->value);break;
    case GE_OP:
      ilhs->value = (ilhs->value >= irhs->value);break;
    case LE_OP:
      ilhs->value = (ilhs->value <= irhs->value);break;
      
    default:
      throw std::invalid_argument("Unknown binary operator");
  }
  return lhs;
}


FRAME_PTR& Interpreter::eval_if_stmt(NODE* ast, FRAME_PTR& env) {
  auto cond = ast->left, body = ast->right;
  auto condVal = std::dynamic_pointer_cast<Int>(eval_expression(cond, env));
  
  if(body->type == ELSE){
    auto wrappedBody = condVal->value ? body->left : body->right;
    return eval(wrappedBody, env);
  } else if (condVal->value )
    return eval(body, env);
  return env;
}

FRAME_PTR& Interpreter::eval_while_loop(NODE* ast, FRAME_PTR& env) {
  auto cond = ast->left, body = ast->right;
  auto condVal = std::dynamic_pointer_cast<Int>(eval_expression(cond, env));
  try{
    while(condVal->value){
      try{
        eval(body, env);
      }catch(Continue& cont){}
      condVal = std::dynamic_pointer_cast<Int>(eval_expression(cond, env));
    }
  }catch(Break& brk){}
  return env;
}

FRAME_PTR& Interpreter::eval_func_def(NODE* ast, FRAME_PTR& env){
  
  auto signature = ast->left, 
       body=ast->right,
       typeAst = signature->left,
       argdefAst = signature->right->right,
       nameAst = signature->right->left;

  IDENT funcName = get_ident(nameAst);
  ARGDEF_T argdefs;
  eval_func_def_args(argdefAst, argdefs);

  auto result = CLOSURE_PTR(new Closure(argdefs, body, env));

  env->bind(funcName, result);
  return env;
}

void Interpreter::eval_func_def_args(NODE* ast, ARGDEF_T& argdefs){
  if(!ast || ast->type == LEAF)
    return;
    
  std::vector<IDENT> argstk;
  while(ast->type != '~') {
    auto left=ast->left, right=ast->right;
    argstk.push_back(get_ident(right->right));
    ast = left;
  }
  argstk.push_back(get_ident(ast->right));

  int argnum = 0; 
  for(auto& arg : reverse(argstk)) 
    argdefs.emplace(argnum++ , arg);
}

VALUE_PTR Interpreter::eval_func_call(NODE* ast, FRAME_PTR& env){
  auto closure = std::dynamic_pointer_cast<Closure>(eval_expression(ast->left, env));
  auto argEnv = FRAME_PTR(new Frame());
  
  eval_func_args(ast->right, closure->argDefs, argEnv, env);
  return call_func(closure, argEnv);
}

void Interpreter::eval_func_args(NODE* ast, ARGDEF_T& argdefs, FRAME_PTR& argenv, FRAME_PTR& env){
  if(!argdefs.size()) {
    if(ast) throw std::invalid_argument("Invalid number of argument");
    return;
  }
  std::vector<NODE*> argstk;
  while(ast->type == ',') {
    argstk.push_back(ast->right);
    ast = ast->left;
  }
  argstk.push_back(ast);

  if(argstk.size() != argdefs.size())
    throw std::invalid_argument("Invalid number of arguments");
  
  int argnum = 0; 
  for(auto& arg: reverse(argstk)) {
    auto argname = argdefs.at(argnum++);
    auto argval = eval_expression(arg, env);
    argenv->bind(argname, argval);
  }
}

VALUE_PTR Interpreter::call_func(CLOSURE_PTR closure, FRAME_PTR& args) {
  try{
    args->setParent(closure->env);
    auto locals = FRAME_PTR(new Frame(args));
    eval(closure->ast, locals);
    return INT_PTR(new Int(0)); 
  }catch(Return& ret){
    return ret.getResult();
  }
}
