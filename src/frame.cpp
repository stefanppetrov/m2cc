#include <frame.h>
#include <values_impl.h>

#include <iostream>
#include <stdexcept>

Frame* Frame::findEnvFor(IDENT ident){
  if(isBound(ident)) 
    return this;
  
  if(parent)
    return parent->findEnvFor(ident);

  return nullptr;
}

bool Frame::isBound(IDENT ident) {
  return bindings.find(ident) != bindings.end();
}

VALUE_PTR& Frame::get(IDENT ident) {
  auto locum = findEnvFor(ident);
  if(!locum)
    throw std::invalid_argument("Unbound identifier: " + ident);
  VALUE_PTR& ptr = locum->bindings.at(ident);
  if(!ptr.get()) {
    throw std::invalid_argument("Uninitiallized variable: " + ident);
  }
  return ptr;
}

void Frame::rebind(IDENT ident, VALUE_PTR value) {
  auto locum = findEnvFor(ident);
  if(!locum)
    throw std::invalid_argument("Unbound identifier: " + ident);
  
  locum->bindings[ident] = value;
}

void Frame::bind(IDENT ident, VALUE_PTR value) {
  bindings.emplace(ident, value);
}

const FRAME_PTR& Frame::getParent() const {
  return parent;
}

void Frame::setParent(FRAME_PTR parent) {
  if(parent.get() == this)
    throw std::invalid_argument("An environment can't be a parent to itself");
  this->parent = parent;
  
}
std::ostream& operator<<(std::ostream& o, const Frame& e) {
  const Frame* ancestor = e.getParent().get(), *previous = &e;
  while(ancestor && ancestor != previous){
    for(const auto& bind: ancestor->bindings) {
      const auto& ptr = bind.second;
      o << bind.first << ": (loc: " << ptr;
      if (ptr.get() && ptr->getType() == INT_T) {
        o << ", value: " << ((Int*)ptr.get())->value;
      }
      o << ")" << "\n";
    }
    previous = ancestor;
    ancestor = ancestor->getParent().get();
    
  }
  for(const auto& bind: e.bindings) {
    const auto& ptr = bind.second;
    o << bind.first << ": (loc: " << ptr;
    if (ptr.get() && ptr->getType() == INT_T) {
      o << ", value: " << ((Int*)ptr.get())->value;
    }
    o << ")" << "\n";
  }
  return o;
}
