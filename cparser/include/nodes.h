#ifndef NODES_H
#define NODES_H

#include "token.h"

typedef struct node {
  int          type;
  struct node *left;
  struct node *right;
} NODE;

#ifdef __cplusplus
  extern "C" {
#endif

NODE* make_leaf(TOKEN*);
NODE* make_node(int, NODE*, NODE*);

#ifdef __cplusplus
 }
#endif

#endif