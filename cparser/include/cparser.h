#ifndef CPARSER_H
#define CPARSER_H

#include "nodes.h"

#ifdef __cplusplus
  extern "C" {
#endif

NODE* parse(int debug);
void print_tree(NODE* ast);
char* named(int tokenType);

#ifdef __cplusplus
  }
#endif

#endif
